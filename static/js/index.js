/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/** */
async function updateFromGet () {
  const error = document.getElementById('error-api')
  const success = document.getElementById('success-api')
  const response = await fetch('/api/index/get')
  console.log(response)
  if (response.status !== 200) {
    error.style.display = 'block'
    success.style.display = 'none'
    console.error('не удалось обновить данные на странице')
    return
  }
  const data = await response.json()
  console.log(data)
  document.getElementById('input-title').value = data.title
  document.getElementById('input-header').value = data.header
  document.getElementById('input-content').value = data.content
  document.getElementById('input-footer').value = data.footer
  document.getElementById('title').innerText = data.title
  document.getElementById('header').innerText = data.header
  document.getElementById('content').innerText = data.content
  document.getElementById('footer').innerText = data.footer
}

/** */
export async function edit () {
  const button = document.getElementById('button-edit')
  const error = document.getElementById('error-api')
  const success = document.getElementById('success-api')
  button.setAttribute('disabled', true)
  const data = {
    id: Number.parseInt(document.getElementById('input-title').dataset.id),
    title: document.getElementById('input-title').value,
    header: document.getElementById('input-header').value,
    content: document.getElementById('input-content').value,
    footer: document.getElementById('input-footer').value
  }
  const url = '/api/index/edit'
  try {
    const response = await fetch(url, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    console.log(response)
    if (response.status !== 200) {
      error.style.display = 'block'
      success.style.display = 'none'
    } else {
      error.style.display = 'none'
      success.style.display = 'block'
      setTimeout(() => { success.style.display = 'none' }, 3000)
    }
    await updateFromGet()
  } catch (err) {
    console.error(err)
  }
  button.removeAttribute('disabled')
}
