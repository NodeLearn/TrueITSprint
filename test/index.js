/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {
  describe,
  before,
  after,
  it
} from 'node:test'
import assert from 'assert'
import { createServer } from '../src/main.js'
import {
  SqliteDatabase,
  Model,
  MockDatabase,
  MockSqliteDatabase
} from '../src/model.js'
import { IndexModel, IndexRecord } from '../src/sqlite/index.js'

class MockIndexModel extends Model {
  db = new MockSqliteDatabase()
}

describe('index routes tests', async () => {
  const server = createServer({})

  before(() => {
    SqliteDatabase.getInstance().setModel(IndexModel.name, new MockIndexModel())
  })

  it('/', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/'
    })
    assert.strictEqual(response.statusCode, 200)
    assert.equal(response.headers['content-type'], 'text/html; charset=utf-8')
    assert.ok(response.body.length !== 0)
  })

  it('/api/index/create', async () => {
    let response = await server.inject({
      method: 'POST',
      url: '/api/index/create',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        title: 'test',
        header: 'test',
        content: 'test',
        footer: 'test'
      }
    })
    assert.strictEqual(response.statusCode, 200)
    assert.equal(response.headers['content-type'], 'application/json; charset=utf-8')
    assert.equal(response.body, '{"id":1}')
    response = await server.inject({
      method: 'POST',
      url: '/api/index/create',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        header: 'test',
        content: 'test',
        footer: 'test'
      }
    })
    assert.strictEqual(response.statusCode, 400)
  })

  it('/api/index/get', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/api/index/get',
      headers: {
        Accept: 'application/json'
      }
    })
    assert.strictEqual(response.statusCode, 200)
    assert.equal(response.headers['content-type'], 'application/json; charset=utf-8')
    assert.deepEqual(JSON.parse(response.body), MockDatabase.data)
  })

  it('/api/index/edit', async () => {
    let response = await server.inject({
      method: 'PUT',
      url: '/api/index/edit',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        id: 1,
        title: 'test',
        header: 'test',
        content: 'test',
        footer: 'test'
      }
    })
    assert.strictEqual(response.statusCode, 200)
    assert.equal(response.headers['content-type'], 'application/json; charset=utf-8')
    assert.equal(response.body, '{"id":1}')
    response = await server.inject({
      method: 'PUT',
      url: '/api/index/edit',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        title: 'test',
        header: 'test',
        content: 'test',
        footer: 'test'
      }
    })
    assert.strictEqual(response.statusCode, 400)
    response = await server.inject({
      method: 'PUT',
      url: '/api/index/edit',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        id: 1,
        title: 'test',
        content: 'test',
        footer: 'test'
      }
    })
    assert.strictEqual(response.statusCode, 400)
  })

  it('/api/index/delete', async () => {
    const response = await server.inject({
      method: 'DELETE',
      url: '/api/index/delete',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: {
        id: 1
      }
    })
    assert.strictEqual(response.statusCode, 200)
  })
})

describe('IndexModel & IndexRecord tests', async () => {
  /**
   * @type {IndexModel}
   */
  let db
  /**
   * @type {IndexRecord}
   */
  let record

  before(async () => {
    db = new IndexModel()
    await db.db.connect()
    record = new IndexRecord(db)
  })

  after(async () => {
    await db.db.close()
  })

  it('test init', async () => {
    await db.init()
  })

  it('test fill', async () => {
    const result = await db.fill()
    assert.equal(result.lastID, 1)
    assert.equal(result.changes, 1)
  })

  it('test create', async () => {
    const result = await record.create('test', 'test', 'test', 'test')
    assert.equal(result.lastID, 2)
    assert.equal(result.changes, 1)
  })

  it('test read', async () => {
    await record.read()
    await record.read(2)
  })

  it('test update', async () => {
    record.title = 'test2'
    const result = await record.update()
    assert.equal(result.lastID, 2)
    assert.equal(result.changes, 1)
  })

  it('test serialize', () => {
    const result = record.toObject()
    assert.deepEqual(result, {
      id: 2,
      title: 'test2',
      header: 'test',
      content: 'test',
      footer: 'test'
    })
  })

  it('test count', async () => {
    const count = await db.count()
    assert.equal(count, 2)
  })

  it('test delete', async () => {
    await record.delete()
    const count = await db.count()
    assert.equal(count, 1)
  })
})
