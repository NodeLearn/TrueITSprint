/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {
  describe,
  before,
  after,
  it
} from 'node:test'
import assert from 'assert'
import { SqliteDatabase } from '../src/model.js'

describe('SqliteDatabase tests', async () => {
  /**
   * @type {SqliteDatabase}
   */
  let db

  before(() => {
    db = SqliteDatabase.getInstance()
  })

  after(async () => {
    await db.close()
  })

  it('test connect', async () => {
    await db.connect()
    const result = await new Promise((resolve, reject) => {
      db.db.run('SELECT 1+1', function (err) {
        if (err) {
          reject(err)
          return
        }
        resolve(this)
      })
    })
    assert.equal(result.lastID, 0)
    assert.equal(result.changes, 0)
  })
})
