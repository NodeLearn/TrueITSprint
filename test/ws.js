/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {
  describe,
  before,
  after,
  it
} from 'node:test'
import assert from 'assert'
// eslint-disable-next-line n/no-unpublished-import
import WebSocket from 'ws'
import { createServer, start } from '../src/main.js'

describe('ws routes tests', async () => {
  let server

  before(async () => {
    server = createServer({})
    start(server)
    await server.ready()
  })

  after(() => server.close())

  it('/ws/hello', { timeout: 3000 }, async () => {
    const client = new WebSocket('ws://localhost:8000/ws/hello')
    client.on('error', err => assert.ifError(err))
    await new Promise(resolve => client.on('open', () => resolve()))
    client.send(Buffer.from('test'))
    const msg = await new Promise(resolve => client.on('message', msg => resolve(msg)))
    assert.equal(msg.toString('utf8'), 'hi from server, test')
    client.close()
  })
})
