/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module sqlite-index
 */
import sqlite3 from 'sqlite3'
import { Model, ModelRecord } from '../model.js'

/**
 * @class
 */
export class IndexModel extends Model {
  static name = 'index'

  /** */
  async init () {
    if (!this.db.isConnected()) throw new Error('db not connected')
    await new Promise((resolve, reject) => {
      this.db.db.exec(
                `
        CREATE TABLE IF NOT EXISTS \`index\` (
          \`id\` INTEGER PRIMARY KEY,
          \`title\` TEXT NOT NULL,
          \`header\` TEXT NOT NULL,
          \`content\` TEXT NOT NULL,
          \`footer\` TEXT NOT NULL
        )
        `,
                err => {
                  if (err) {
                    reject(err)
                  } else {
                    resolve()
                  }
                }
      )
    })
  }

  /**
   * @returns {Promise<sqlite3.RunResult>} result
   */
  async fill () {
    if (!this.db.isConnected()) throw new Error('db not connected')
    const result = await new Promise((resolve, reject) => {
      this.db.db.run(
        'INSERT INTO `index` (`title`, `header`, `content`, `footer`) VALUES (?, ?, ?, ?)',
        ['Index page', 'Test header title', 'Test content value', 'Test footer'],
        function (err) {
          if (err) {
            reject(err)
            return
          }
          resolve(this)
        }
      )
    })
    return result
  }

  /**
   * @returns {Promise<number>} rows count
   */
  async count () {
    if (!this.db.isConnected()) throw new Error('db not connected')
    const result = await new Promise((resolve, reject) => {
      this.db.db.each('SELECT Count(*) FROM `index`', (err, row) => {
        if (err) {
          reject(err)
          return
        }
        resolve(row['Count(*)'])
      })
    })
    return result
  }
}

/**
 * @class
 */
export class IndexRecord extends ModelRecord {
  /**
   * @private
   */
  _id
  /**
   * @type {string}
   */
  title
  /**
   * @type {string}
   */
  header
  /**
   * @type {string}
   */
  content
  /**
   * @type {string}
   */
  footer

  /**
   * @param {...string} args 4 args: title, header, content, footer
   * @returns {Promise<sqlite3.RunResult>} result
   */
  async create (...args) {
    if (!this.model.db.isConnected()) throw new Error('db not connected')
    if (args.length !== 4) throw new Error('lengh is not 4')
    for (const val in args) {
      if (typeof val !== 'string') throw new Error('all values must be string')
    }
    const result = await new Promise((resolve, reject) => {
      this.db.run(
        'INSERT INTO `index` (`title`, `header`, `content`, `footer`) VALUES (?, ?, ?, ?)',
        args,
        function (err) {
          if (err) {
            reject(err)
            return
          }
          resolve(this)
        }
      )
    })
    return result
  }

  /**
   * read random record and fill fields
   * @param {number} id get row by id if this param not null
   */
  async read (id) {
    if (!this.model.db.isConnected()) throw new Error('db not connected')
    if (id && typeof id !== 'number') throw new Error('id is not number')
    if (id && id < 1) throw new Error('id is < 1')
    let query = 'SELECT * FROM `index` ORDER BY RANDOM() LIMIT 1'
    if (id) {
      query = `SELECT * FROM \`index\` WHERE id = ${id} LIMIT 1`
    }
    await new Promise((resolve, reject) => {
      this.db.each(query, (err, row) => {
        if (err) {
          reject(err)
          return
        }
        this._id = row.id
        this.title = row.title
        this.header = row.header
        this.content = row.content
        this.footer = row.footer
        resolve()
      })
    })
  }

  /**
   * @returns {Promise<sqlite3.RunResult>} result
   */
  async update () {
    if (!this.model.db.isConnected()) throw new Error('db not connected')
    if (this._id == null) throw new Error('record is not readed')
    const result = await new Promise((resolve, reject) => {
      this.db.run(
        'UPDATE `index` SET `title` = ?, `header` = ?, `content` = ?, `footer` = ? WHERE `id` = ?',
        [
          this.title,
          this.header,
          this.content,
          this.footer,
          this._id
        ],
        function (err) {
          if (err) {
            reject(err)
          } else {
            resolve(this)
          }
        }
      )
    })
    return result
  }

  /**
   * delete this record
   */
  async delete () {
    if (!this.model.db.isConnected()) throw new Error('db not connected')
    if (this._id == null) throw new Error('record is not readed')
    await new Promise((resolve, reject) => {
      this.db.exec(`DELETE FROM \`index\` WHERE \`id\` = ${this._id}`, err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }

  /**
   * serialize to object
   * @returns {object} data
   */
  toObject () {
    return {
      id: this._id,
      title: this.title,
      header: this.header,
      content: this.content,
      footer: this.footer
    }
  }

  /**
   * @returns {number} id
   */
  getID () {
    return this._id
  }
}
