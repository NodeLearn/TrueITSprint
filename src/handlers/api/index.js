/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module handler-api-index
 */
import fastify from 'fastify'
import lodash from 'lodash'
import { SqliteDatabase } from '../../model.js'
import { IndexModel, IndexRecord } from '../../sqlite/index.js'

SqliteDatabase.getInstance().setModel(IndexModel.name, new IndexModel())

/**
 * @param {fastify.FastifyRequest} req request
 * @param {fastify.FastifyReply} rep reply
 * @returns {object} { id: number }
 */
export async function createHandler (req, rep) {
  const record = new IndexRecord(SqliteDatabase.getInstance().getModel(IndexModel.name))
  const result = await record.create(
    lodash.escape(req.body.title),
    lodash.escape(req.body.header),
    lodash.escape(req.body.content),
    lodash.escape(req.body.footer)
  )
  return { id: result.lastID }
}

/**
 * @param {fastify.FastifyRequest} req request
 * @param {fastify.FastifyReply} rep reply
 * @returns {object} random IndexRecord object
 */
export async function readHandler (req, rep) {
  const record = new IndexRecord(SqliteDatabase.getInstance().getModel(IndexModel.name))
  await record.read()
  return record.toObject()
}

/**
 * @param {fastify.FastifyRequest} req request
 * @param {fastify.FastifyReply} rep reply
 * @returns {object} { id: number }
 */
export async function updateHandler (req, rep) {
  const record = new IndexRecord(SqliteDatabase.getInstance().getModel(IndexModel.name))
  const id = req.body.id
  if (id && id < 1) {
    rep.status(400)
    return { error: 'id is < 1' }
  }
  await record.read(id)
  record.title = lodash.escape(req.body.title)
  record.header = lodash.escape(req.body.header)
  record.content = lodash.escape(req.body.content)
  record.footer = lodash.escape(req.body.footer)
  const result = await record.update()
  return { id: result.lastID }
}

/**
 * @param {fastify.FastifyRequest} req request
 * @param {fastify.FastifyReply} rep reply
 * @returns {*} any
 */
export async function deleteHandler (req, rep) {
  const record = new IndexRecord(SqliteDatabase.getInstance().getModel(IndexModel.name))
  const id = req.body.id
  if (id && id < 1) {
    rep.status(400)
    return { error: 'id is < 1' }
  }
  await record.read(id)
  await record.delete()
}
