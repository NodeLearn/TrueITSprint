/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module model
 */
import sqlite3 from 'sqlite3'

/**
 * db wrapper
 * @class
 */
export class SqliteDatabase {
  /**
   * @private
   * @type {SqliteDatabase}
   */
  static _instance
  /**
   * @type {sqlite3.Database}
   */
  db
  /**
   * @private
   * @type {boolean}
   */
  _connected = false
  /**
   * @private
   * @type {Map<string, Model>}
   */
  _models = new Map()

  /**
   * (create and) get instance
   * @returns {SqliteDatabase} instance
   */
  static getInstance () {
    if (SqliteDatabase._instance == null) {
      SqliteDatabase._instance = new SqliteDatabase()
    }
    return SqliteDatabase._instance
  }

  /**
   * connect to `:memory:`
   */
  async connect () {
    if (this.isConnected()) return
    await new Promise((resolve, reject) => {
      this.db = new sqlite3.Database(':memory:', err => {
        if (err) {
          reject(err)
          return
        }
        this._connected = true
        resolve()
      })
    })
  }

  /**
   * close db connection
   */
  async close () {
    if (!this.isConnected()) return
    await new Promise((resolve, reject) => {
      this.db.close((err) => {
        if (err) {
          reject(err)
          return
        }
        this._connected = false
        resolve()
      })
    })
  }

  /**
   * @returns {boolean} true if connected
   */
  isConnected () {
    return this._connected
  }

  /**
   * @param {string} name model name
   * @param {Model} model instance
   */
  setModel (name, model) {
    this._models.set(name, model)
  }

  /**
   * @param {string} name model name
   * @returns {Model | undefined} model instance
   */
  getModel (name) {
    return this._models.get(name)
  }

  /**
   * call `init` and `fill` for all of registered models
   */
  async initModels () {
    for (const model of this._models.values()) {
      await model.init()
      await model.fill()
    }
  }
}

/**
 * abstract class for extends
 * @class
 */
export class Model {
  /**
   * @type {SqliteDatabase}
   */
  db = SqliteDatabase.getInstance()
  /**
   * name model, must be override
   * @type {string}
   */
  static name = ''

  /**
   * create (only one) table
   * @abstract
   */
  async init () {
    throw new Error('Method not implemented.')
  }

  /**
   * create records
   * @abstract
   * @returns {Promise<sqlite3.RunResult>} result
   */
  async fill () {
    throw new Error('Method not implemented.')
  }

  /**
   * get number of rows
   * @abstract
   * @returns {Promise<number>} rows count
   */
  async count () {
    throw new Error('Method not implemented.')
  }
}

/**
 * abstract class for extends by custom model representation
 * @class
 */
export class ModelRecord {
  /**
   * @type {Model}
   */
  model
  /**
   * @type {sqlite3.Database}
   */
  db

  /**
   * @param {Model} model custom model instance
   */
  constructor (model) {
    this.model = model
    this.db = model.db.db
  }

  /**
   * create record in db
   * @param  {...any} args data for create record
   * @returns {Promise<sqlite3.RunResult>} result
   * @abstract
   */
  async create (...args) {
    throw new Error('Method not implemented.')
  }

  /**
   * get record from db and fill class intance fields
   * @param  {...any} args data for search in db
   * @abstract
   */
  async read (...args) {
    throw new Error('Method not implemented.')
  }

  /**
   * update record in db
   * @param  {...any} args data for update record
   * @returns {Promise<sqlite3.RunResult>} result
   * @abstract
   */
  async update (...args) {
    throw new Error('Method not implemented.')
  }

  /**
   * delete record from db
   * @abstract
   */
  async delete () {
    throw new Error('Method not implemented.')
  }

  /**
   * serialize instance to object representation
   * @abstract
   * @returns {object} serialized data
   */
  toObject () {
    throw new Error('Method not implemented.')
  }
}

/**
 * mock for unit tests, replace for `sqlite3.Database`
 * @class
 */
export class MockDatabase {
  /** */
  static data = {
    id: 1,
    title: 'Test mock title',
    header: 'test',
    content: 'test',
    footer: 'test'
  }

  /**
   * @param {string} query sql
   * @param {Function} callback function
   */
  each (query, callback) {
    callback(null, MockDatabase.data)
  }

  /**
   * @param {string} query sql
   * @param {Array<any>} args args
   * @param {Function} callback function
   */
  run (query, args, callback) {
    callback.call({
      lastID: 1,
      changes: 1
    }, null)
  }

  /**
   * @param {string} query sql
   * @param {Function} callback function
   */
  exec (query, callback) {
    callback(null)
  }
}

/**
 * mock for unit tests, replace for `SqliteDatabase`
 * @class
 */
export class MockSqliteDatabase {
  /**
   * @type {MockDatabase}
   */
  db = new MockDatabase()

  /**
   * @returns {boolean} true
   */
  isConnected () {
    return true
  }
}
