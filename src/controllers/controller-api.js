/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module controller-api
 */
import fastify from 'fastify'
import indexRoutes from './api/index.js'

/**
 * register routes for REST API
 * @param {fastify.FastifyInstance} fastify instance of server
 */
export async function declareAPIRoutes (fastify) {
  await indexRoutes(fastify)
}
