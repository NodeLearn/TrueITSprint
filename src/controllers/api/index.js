/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module controller-api-index
 */
import fastify from 'fastify'
import {
  createHandler,
  readHandler,
  updateHandler,
  deleteHandler
} from '../../handlers/api/index.js'

/**
 * @param {fastify.FastifyInstance} fastify instance of server
 */
export default async function (fastify) {
  fastify.route({
    method: 'POST',
    url: '/index/create',
    handler: createHandler,
    schema: {
      produces: ['application/json'],
      tags: ['api'],
      description: 'index record create',
      body: {
        type: 'object',
        required: [
          'title',
          'header',
          'content',
          'footer'
        ],
        properties: {
          title: { type: 'string' },
          header: { type: 'string' },
          content: { type: 'string' },
          footer: { type: 'string' }
        }
      },
      response: {
        default: {
          description: 'index page',
          type: 'object',
          content: {
            'application/json': {
              schema: {
                id: { type: 'number' }
              }
            }
          }
        }
      }
    }
  })
  fastify.route({
    method: 'GET',
    url: '/index/get',
    handler: readHandler,
    schema: {
      produces: ['application/json'],
      tags: ['api'],
      description: 'index record read',
      response: {
        default: {
          description: 'index page data',
          type: 'object',
          content: {
            'application/json': {
              schema: {
                id: { type: 'number' },
                title: { type: 'string' },
                header: { type: 'string' },
                content: { type: 'string' },
                footer: { type: 'string' }
              }
            }
          }
        }
      }
    }
  })
  fastify.route({
    method: 'PUT',
    url: '/index/edit',
    handler: updateHandler,
    schema: {
      produces: ['application/json'],
      tags: ['api'],
      description: 'index record update',
      body: {
        type: 'object',
        required: [
          'id',
          'title',
          'header',
          'content',
          'footer'
        ],
        properties: {
          id: { type: 'number' },
          title: { type: 'string' },
          header: { type: 'string' },
          content: { type: 'string' },
          footer: { type: 'string' }
        }
      },
      response: {
        default: {
          description: 'index page update data',
          type: 'object',
          content: {
            'application/json': {
              schema: {
                id: { type: 'number' }
              }
            }
          }
        }
      }
    }
  })
  fastify.route({
    method: 'DELETE',
    url: '/index/delete',
    handler: deleteHandler,
    schema: {
      produces: ['application/json'],
      tags: ['api'],
      description: 'index record create',
      body: {
        type: 'object',
        properties: {
          id: { type: 'number' }
        }
      }
    }
  })
}
