/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module controller-ws
 */
import fastify from 'fastify'
import { helloWorld } from '../handlers/ws/hello.js'

/**
 * register routes for WebSocket
 * @param {fastify.FastifyInstance} fastify instance of server
 */
export async function declareWsRoutes (fastify) {
  fastify.route({
    method: 'GET',
    websocket: true,
    url: '/hello',
    handler: helloWorld,
    schema: {
      produces: ['text/plain'],
      tags: ['ws'],
      description: 'hello, world',
      response: {
        default: {
          description: 'hello, world',
          type: 'string'
        }
      }
    }
  })
}
