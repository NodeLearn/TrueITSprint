/*
Copyright 2023 snegg21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @module main
 */
import { exit } from 'node:process'
import path from 'path'
import fastify from 'fastify'
import fastifyView from '@fastify/view'
import handlebars from 'handlebars'
import fastifyHelmet from '@fastify/helmet'
import fastifyCookie from '@fastify/cookie'
import fastifyJwt from '@fastify/jwt'
import fastifyStatic from '@fastify/static'
import websocketPlugin from '@fastify/websocket'
import fastifySwagger from '@fastify/swagger'
import fastifySwaggerUi from '@fastify/swagger-ui'
import { SqliteDatabase } from './model.js'
import { declareWsRoutes } from './controllers/controller-ws.js'
import { declareHTMLRoutes } from './controllers/controller-html.js'
import { declareAPIRoutes } from './controllers/controller-api.js'

/**
 * options for fastify server
 * @type {fastify.FastifyServerOptions}
 */
export const options = {
  logger: true
}

/**
 * create and setup fastify server
 * @param {fastify.FastifyServerOptions} options FastifyOptions
 * @returns {fastify.FastifyInstance} server
 */
export function createServer (options) {
  const server = fastify(options)

  server.register(fastifyView, {
    engine: {
      handlebars
    }
  })
  server.register(fastifyHelmet, { contentSecurityPolicy: false })
  server.register(fastifyCookie, {
    secret: 'my-secret',
    hook: 'onRequest',
    parseOptions: {}
  })
  server.register(fastifyJwt, {
    secret: 'supersecret'
  })
  server.register(fastifyStatic, {
    root: path.resolve('./static'),
    prefix: '/static'
  })
  server.register(websocketPlugin)
  server.register(fastifySwagger, {
    swagger: {
      info: {
        title: 'TrueItSprint API',
        description: 'Testing the TrueItSprint API',
        version: '0.0.1'
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here'
      },
      host: 'localhost:8000',
      schemes: ['http'],
      consumes: ['application/json', 'text/plain'],
      produces: ['application/json', 'text/html', 'text/plain'],
      tags: [
        { name: 'http', description: 'html pages' },
        { name: 'api', description: 'REST API end-points' },
        { name: 'ws', description: 'WebSocket' }
      ]
    }
  })
  server.register(fastifySwaggerUi, {
    routePrefix: '/docs',
    uiConfig: {
      docExpansion: 'full',
      deepLinking: false
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    transformSpecification: (swaggerObject) => { return swaggerObject },
    transformSpecificationClone: true
  })
  server.setErrorHandler(function (err, req, rep) {
    this.log.error(err)
    rep.status(err.statusCode).send({
      error: err.message,
      stack: err.stack,
      status: err.statusCode
    })
  })

  server.register(declareWsRoutes, { prefix: '/ws' })
  server.register(declareAPIRoutes, { prefix: '/api' })
  server.register(declareHTMLRoutes)

  return server
}

/**
 * connect to db and init models, must call after `createServer`
 */
export async function initDatabase () {
  const db = SqliteDatabase.getInstance()
  await db.connect()
  await db.initModels()
}

/**
 * start HTTP server
 * @param {fastify.FastifyInstance} server FastifyInstance
 * @param {number} port port for listen (default is 8000)
 */
export async function start (server, port) {
  try {
    await server.listen({
      host: '0.0.0.0',
      port: port | 8000
    })
  } catch (err) {
    server.log.error(err)
    exit(1)
  }
}
