# TrueITSprint

![logo](static/images/logo.svg)

Этот проект предназначается для обучения основам NodeJS (JavaScript), SQL и вёрстки (HTML и CSS).

## npm команды

* `npm lint` — проверка на стилистические ошибки
* `npm fix` — исправление ошибок (если возможно)
* `npm test` — запуск юнит-тестов
* `npm jsgl` — проверка лицензий зависимостей
* `npm jsdoc` — генерация документации по коду
* `npm docs` — запуск веб-сервера с документацией
* `npm clean` — очистка документации
* `npm start` — запуск сервера

Сервер запускается на порту 8000. Пример ссылки: [http://localhost:8000/](http://localhost:8000/).
Можно посмотреть документацию по запросам к API: [http://localhost:8000/docs](http://localhost:8000/docs).

## git & linux & ssh

```sh
# настройка git
git config --global user.name "your_username"
git config --global user.email "your-mail@example.com"
git config --global core.editor "nano"
# инициализация нового локального репозитория
git init
# добавление удалённого репозитория
git remote add origin git@codeberg.org:NodeLearn/TrueITSprint.git
# скачивание кода из основной ветки
git pull origin master
# создание ветки
git checkout -b your_branch
# написание кода
# и коммит
git add .
git commit -m "add my feature"
# генерация ssh ключа
ssh-keygen -t ed25519 -C "your-mail@example.com"
# вывод публичного ключа в терминал
# и добавление его в настройках
cat ~/.ssh/your_file.pub
# запуск ssh агента
eval $(ssh-agent -s)
# добавление в него приватного ключа
ssh-add ~/.ssh/your_file
# отправка изменений в репозиторий
git push origin your_branch
```

## Структура проекта

* `./src/main.js` — главный файл проекта, содержащий функции создания и инициализации сервера
* `./src/model.js` — файл с классами для работы с базой данных и наследованием от них
* `./src/start.js` — запускатор, использует `main.js`
* `./src/controllers` — директория с контроллерами (описание маршрутов (URL-адресов, энд-поинтов) веб-сервера)
* `./src/controllers/controller-api.js` — контроллер для REST API
* `./src/controllers/controller-html.js` — контроллер для страниц в HTML
* `./src/controllers/controller-ws.js` — контроллер для веб-сокетов
* `./src/controllers/api` — директория для контроллеров REST API, чтобы разделить функционал по отдельным модулям
* `./src/handlers` — хандлеры (обработчики) для запросов
* `./src/handlers/api` — обработчики для REST API
* `./src/handlers/html` — обработчики для HTML
* `./src/handlers/ws` — обработчики для веб-сокетов
* `./src/sqlite` — директория для моделей
* `./static` — директория для статических файлов
* `./static/css` — директория для файлов стилей CSS
* `./static/images` — директория для изображений
* `./static/js` — директория для клиентского JavaScript, который выполняется в браузере
* `./test` — директория для юнит-тестов

## Инструменты

* [vscode](https://code.visualstudio.com/): редактор
* [git](https://git-scm.com/downloads)
* [NodeJS](https://nodejs.org/en/download/)

### Расширения vscode

* [ESLint](https://open-vsx.org/vscode/item?itemName=dbaeumer.vscode-eslint)
* [Stylelint](https://open-vsx.org/vscode/item?itemName=stylelint.vscode-stylelint)
* [Git Extension Pack](https://open-vsx.org/vscode/item?itemName=sugatoray.vscode-git-extension-pack)

## Полезные ресурсы

* *[WSL](https://learn.microsoft.com/ru-ru/windows/wsl/tutorials/wsl-vscode): для терминала Linux*
* [Bing](https://www.bing.com/): поисковик с ИИ чатом
* [ChatGBT бот](https://t.me/GPT_onlinebot)
* [Sqlite tutorial](https://www.sqlitetutorial.net/)
* [Учебник JavaScript, HTML, CSS, SQL](https://www.schoolsw3.com/index.php)
* [Учебник по всему](https://code.mu/ru/): JavaScript, NodeJS, вёрстка
* [Документация NodeJS](https://nodejs.org/dist/latest-v20.x/docs/api/)
* [Учебник JavaScript](https://learn.javascript.ru/)
* [Справочник по HTML и CSS](https://htmlbook.ru/)
* [Гайд по flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [Онлайн редактор](https://jsfiddle.net/): можно писать вместе и делится
* [Colorhunt](https://colorhunt.co/): цветовые гаммы
* [Генератор иконок](https://perchance.org/ai-icon-generator)
* [Бот для генерации картинок](https://t.me/stable_diffusion_bot)
* [Учебник git](https://githowto.com/ru)
* [Книга про git](https://git-scm.com/book/ru/v2)
* [Соглашение о коммитах](https://www.conventionalcommits.org/ru/v1.0.0/) (желательно соблюдать)
* [Учебник по shell (командная строка Linux)](https://www.learnshell.org/) (на английском)
* *[Учебник Docker](https://learntutorials.net/ru/docker/topic/658/%d0%bd%d0%b0%d1%87%d0%b0%d0%bb%d0%be-%d1%80%d0%b0%d0%b1%d0%be%d1%82%d1%8b-%d1%81-docker)*
* [HTTP заголовки](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%B7%D0%B0%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BA%D0%BE%D0%B2_HTTP)
* [HTTP коды состояния](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BA%D0%BE%D0%B4%D0%BE%D0%B2_%D1%81%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D1%8F_HTTP)
* [MIME типы](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_MIME-%D1%82%D0%B8%D0%BF%D0%BE%D0%B2)

### Книги

Курсивом выделены те книги, которые не актуальны для данного проекта, но всё равно полезны.

* Бэн Фрэйн: Отзывчивый дизайн на HTML5 и CSS3 для любых устройств, 3 издание
* Майрейн Хавербеке: Выразительный JavaScript, 3 издание
* *Дэн Вандеркам: Эффективный TypeScript, 2020*
* Уильям Шоттс: Командная строка Linux. Полное руководство, 2017
* *Михаэль Кофлер: Linux. Установка, настройка, администрирование, 2014*

### Принципы программирования

* [Методология БЭМ](https://ru.bem.info/methodology/)
* [KISS](https://ru.wikipedia.org/wiki/KISS_(%D0%BF%D1%80%D0%B8%D0%BD%D1%86%D0%B8%D0%BF))
* [SOLID](https://ru.wikipedia.org/wiki/SOLID_(%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5))
* [Контрактное программирование](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BD%D1%82%D1%80%D0%B0%D0%BA%D1%82%D0%BD%D0%BE%D0%B5_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5#%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F_%D0%B2_%D1%8F%D0%B7%D1%8B%D0%BA%D0%B0%D1%85_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)
* [GRASP паттерны](https://ru.wikipedia.org/wiki/GRASP)
* [GoF паттерны](https://ru.wikipedia.org/wiki/Design_Patterns)

### Ещё книги

Данные книги не нужно читать сразу, но они пригодяться позднее, чтобы повысить квалификацию.
Они предназначены для тех, у кого уже есть ~1 год опыта в программировании.

* Адитья Бхаргава: Грокаем алгоритмы
* Мартин Фаулер: Рефакторинг. Улучшение проекта существующего кода
* Роберт Мартин: Чистый код. Создание, анализ и рефакторинг
* Дж. Ханк Рейнвотер: Как пасти котов

## Docker

В проекте сконфигурирован докер, но его использование не обязательно. Это просто пример для желающих
изучить эту технологию.

```sh
# сборка контейнера
sudo docker-compose build
# сборка без кэша, может пригодится в некоторых случаях
sudo docker-compose build --pull --no-cache
# запуск
sudo docker-compose up
# запуск в фоне
sudo docker-compose up -d
# остановка всех контейнеров
sudo docker ps -aq | sudo xargs docker stop
# или только этого проекта
sudo docker-compose stop

```

## Вклад в развитие (contributing)

* Проект использует правила оформления кода [standard](https://standardjs.com/rules). Для удобства
используются линтеры ESLint и StyleLint. Код не должен содержать ошибок согласно этим линтерам.
* Работа должна вестись в отдельной ветке. Например: `feature/palindrome`. Когда работа готова,
необходимо оформить pull request (запрос на слияние) в основную ветку.
* Желательно писать содержательные сообщения коммитов и использовать 1 коммит для 1 изменения.
Например: 1 коммит для добавления handlebars шаблона и CSS файла, 1 для написания хандлеров,
1 для БД, 1 для юнит-тестов и так далее.
* Допускается использовать русский язык в документации и комментариях.
* Все наименования в коде строго на понятном английском языке с соблюдением регистра.
* Обязательно использовать заголовок с уведомлением о лицензионном соглашении. Пример копирайта:
`Copyright 2023 John Doe <mail@example.com>`. Можно использовать просто ник: `Copyright 2023 snegg21`.
Каждый, кто редактировал исходный код файла, добавляет свой копирайт отдельной строкой.
* Обязательно писать документацию к модулям, классам, методам, функциям, полям. Ко всем конструкциям,
которые не приватные.
* Обязательно описывать схему запросов для swagger.
* Обязательно покрытие юнит-тестами.
* Обязательно тестировать исключительные случаи (обработку ошибок) для REST API.
* Обязательно писать код безопасно, со всеми необходимыми проверками
и экранированием строк (где это нужно).
* Запрос на слияние не будет принят до тех пор, пока код не будет приведён к соответствию
всем стандартам качества.
* Копирование стороннего когда в проект допустимо только в том случае, если лицензия совместима
с Apache 2.0, под которой лицензируется данный проект.
* Изображения и прочие файлы, добавляемые в проект, должны принадлежать вам, либо быть свободными.
* Добавление новых зависимостей нежелательно. У проекта уже есть все необходимые зависимости.

В качестве примера нужно использовать реализацию главной страницы (index). Файлы реализации:

* `./src/controllers/api/index.js`
* `./src/controllers/controller-api.js`
* `./src/controller/controller-html.js`
* `./src/handlers/api/index.js`
* `./src/handlers/html/index.js`
* `./src/sqlite/index.js`
* `./test/index.js`
* `./templates/index.hbs`
* `./static/css/index.css`
* `./static/js/index.js`

## Лицензионное соглашение

Исходный код распространяется по условиям [Apache-2.0](https://spdx.org/licenses/Apache-2.0.html),
изображения и прочие двоичные файлы по условиям [CC0-1.0](https://spdx.org/licenses/CC0-1.0.html), тексты лиценщий
предоставлены в директории `LICENSES`.
