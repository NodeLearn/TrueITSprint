FROM node:latest as build-stage
WORKDIR /app
COPY . .
RUN npm i

FROM build-stage AS run-test-stage
WORKDIR /app
RUN npm run lint
RUN npm run test

FROM run-test-stage as build-release-stage
WORKDIR /app
EXPOSE 8000
USER daemon:daemon
ENTRYPOINT [ "node", "/app/src/start.js" ]
